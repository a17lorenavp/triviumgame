import React from 'react';
import logo from './logo.svg';
import './App.css';
import Dashboard from './components/Dashboard';
import Header from './components/Header';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Perfil from './components/Perfil';
import Partidas from './components/Partidas';
import NewUSer from './components/NewUser';
import { Link } from 'react-router-dom';
import Login from './components/Login';
import Partida from './components/Partida';
import Button from '@material-ui/core/Button';
import MaterialTheme from './components/MaterialTheme';


function App() {
  const [isLogged, setIsLogged] = React.useState(localStorage.getItem('trivialToken'));
  
  return (
    <MaterialTheme>
      <div className="App" style={{}}>
        <header className="App-header">
          <Router>
          {isLogged?
            <>
              <Header
                isLogged={isLogged}
                logout={() => {
                  setIsLogged(false);
                  localStorage.removeItem('trivialToken');
                }}

              />
              <Switch>
                <Route exact path={['/']} render={() => (
                  <>
                    <Dashboard 
                      logout={() => {
                        setIsLogged(false);
                        localStorage.removeItem('trivialToken');
                      }}
                    />
                  </>
                )} />
                <Route path={['/account/:userId']} component={Perfil} />
                <Route path={['/partidas']} component={Partidas} />
                <Route path={['/partida/:idPartida']} component={Partida} />

              </Switch>
            
            </>
          :
            
            <Switch>
              <Route exact path={['/']} component={() => (
                <>
                <Login setIsLogged={setIsLogged}></Login>
                </>
              )} />
              <Route path={['/newUser']} component={NewUSer} />
            </Switch>
            
          }
          </Router>      
          
        </header>
      </div>
    </MaterialTheme>
  );
}


export default App;
