import React from 'react';
import Marco from './Marco';
import { Link } from 'react-router-dom';
import Pregunta from './Pregunta';


const Partida = ({ match }) => {
    console.log(match);
    const [data, setData] = React.useState(null);

    const getDatos = async () => {
        const response = await fetch('http://localhost:5000/partida/' + match.params.idPartida, {
            headers: {
              'token-jibiri': localStorage.getItem('trivialToken')
            }
          });

          if(response.status === 200){
            const json = await response.json();
            setData(json);
          }
    }

    React.useEffect(() => {
        getDatos();
    }, [])
    if(!data){
        return 'loading';
    }
    return (
        <>
            <Marco>
                <Pregunta partida={data} />
            </Marco>
         </>
    )
}

export default Partida;