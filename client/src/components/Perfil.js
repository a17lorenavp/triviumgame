import { Button, Card, CardContent, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import Marco from './Marco';
import NewPartida from './NewPartida';

const Perfil = ({ match }) => {
    
    const [data, setData] = React.useState(null);

    const editar = async () => {
        return (
            <div>
                Cambiar foto de perfil:
                Cambiar nombre:
                cambiar contraseña:
            </div>
        );
    }

    const getDatos = async () => {
        const response = await fetch('http://localhost:5000/perfil', {
            headers: {
              'token-jibiri': localStorage.getItem('trivialToken')
              //'Content-type': 'application/json',
            }
          });
          if(response.status === 200){
            const json = await response.json();
            setData(json);
          }

    }

    React.useEffect(() => {
        getDatos();
    }, [])
    return (
        <>
            <Marco>
                <div style={{
                    width:'100%',
                    display:'flex',
                    justifyContent:'center',
                    height:'100%'
                }}>
                    <Card style={{
                        marginTop:'1.5em',
                        width:'60%', 
                        height:'calc(100% - 3em)'
                    }}>
                        <CardContent style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent:'center',
                        }}>
                            <Typography variant="h4" gutterBottom style={{color: '#386F85'}}>
                                Foto de perfil
                            </Typography>
                            <Typography variant="h4" gutterBottom style={{color: '#386F85'}}>
                                {data && data.username}
                            </Typography>

                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <Button  variant="contained" size="large" color="primary" onClick={editar} fullWidth>EDITAR PERFIL</Button>
                                </Grid> 
                            </Grid>
                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <Link className='link' to='/partidas'>
                                        <Button  variant="contained" size="large" color="primary" fullWidth>VER MIS PARTIDAS</Button>
                                    </Link>
                                    
                                </Grid> 
                            </Grid>  
                            <NewPartida />
                        </CardContent>
                    </Card>
                </div>
            </Marco>
        </>
    )
}

export default Perfil;