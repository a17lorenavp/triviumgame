import React from 'react';
import Perfil from './Perfil';
import { findByLabelText } from '@testing-library/react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';



const Header = ({ logout, isLogged }) => {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    const handleClose = () => {
      setAnchorEl(null);
    };


    return (
        <div style = {{
            width: '100%',
            height: '3.5em',
            backgroundColor: 'white', 
            display: 'flex', 
            flexDirection: 'row', 
            justifyContent: 'space-between',
            alignItems: 'center',
            borderBottom: '1px solid gainsboro'
        }}>
            <Link to="/" style = {{
                textDecoration: 'none',
                color: '#386F85'
            }}>
                <div
                    style={{
                        marginLeft: '.5em',
                        fontFamily: 'Kaushan Script, cursive',
                        fontSize: '1.5em',
                    }}
                >
                    <Link className='link' to="/">TriviumGame</Link>
                </div>
            </Link>

            <div style = {{
                display: 'flex',
                marginRight: '.5em'
            }}>
                {isLogged && 
                    <>

                        <Button onClick={handleClick} color='primary'>
                            <span className="material-icons">
                                menu
                            </span>
                        </Button>
                        <Menu
                            id="simple-menu"
                            anchorEl={anchorEl}
                            keepMounted
                            open={Boolean(anchorEl)}
                            onClose={handleClose}
                        >
                            <Link className='link' to="/account/23"><MenuItem>Perfil</MenuItem></Link>
                            <MenuItem onClick={logout}>Logout</MenuItem>
                        </Menu>
                     </>
                }
                
            </div>
        </div>
    )
}

export default Header;