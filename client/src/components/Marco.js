import React from 'react';
import logo from '../assets/img/fondoMarcoEdit.png';

const Marco = ({ children}) => {

    return (
        <div style={{
            width: '100%',
            height: 'calc(100vh - 3.5em)',
            background: `url(${logo})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
        }}>
            {children}
        </div>
    )
}

export default Marco;