import React from 'react';
import Spinner from 'react-spinkit';
import Marco from './Marco';
import { Link } from 'react-router-dom';


const Partidas = ({ match }) => {
    const [partidas, setPartidas] = React.useState(null);
    const [loading, setLoading] = React.useState(true);

    const pedirPartidas = async () => {
        const response = await fetch('http://localhost:5000/partidas', {
            headers: {
                'token-jibiri': localStorage.getItem('trivialToken')
            }
        });
        
        if(response.status === 200){
            const json = await response.json();
            setPartidas(json.partidas);
        }
        setLoading(false);
    }

    React.useEffect(() => {
        pedirPartidas();
    }, [])

    return (
        <Marco color='blue'>
            {loading?
                <Spinner name="ball-zig-zag" color="white" style={{ marginTop: '5em'}} />
            :
                partidas.length > 0?
                
                    partidas.map(partida => (
                        <div
                            style={{
                                width: '100%',
                                border: '1px solid white',
                                marginTop: '1em'
                            }}
                        >
                            <Link to={`/partida/${partida._id}`}>{partida._id}</Link>
                        </div>
                    ))
                :
                    'No hay partidas'
            }
            <Link to="/">
                <button style={{
                    borderRadius: '5px',
                    padding: '16px'
                }}>
                    VOLVER
                </button>
            </Link>
        </Marco>
    )
}

export default Partidas;