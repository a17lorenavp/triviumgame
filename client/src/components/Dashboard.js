import React from 'react';
import { Router } from 'react-router-dom';
import { Link } from 'react-router-dom';
import NewPartida from './NewPartida';
import Marco from './Marco';
import { Card, CardContent, Typography } from '@material-ui/core';


const Dashboard = ({ logout }) => {
    const [data, setData] = React.useState(null);
    const [loading, setLoading] = React.useState(false);

    const pedir = async () => {
        setLoading(true);
        const response = await fetch('http://localhost:5000/perfil', {
          headers: {
            'token-jibiri': localStorage.getItem('trivialToken')
            //'Content-type': 'application/json',
          }
        });
        
        if(response.status === 200){
          const json = await response.json();
          setData(json);
        }
    
        setLoading(false);
    }

    React.useEffect(() => {
      pedir();
    }, [])
    return (
      <>
        <Marco>
          <div style={{
            width:'100%',
            display:'flex',
            justifyContent:'center',
            height:'50%'
          }}>
            <Card style={{
              marginTop:'1.5em',
              width:'60%', 
              height:'calc(100% - 3em)'
            }}>
              <CardContent style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent:'center',
              }}>
                <Typography variant="h3" gutterBottom style={{color: '#386F85'}}>
                   ¡Vamos a jugar,  {data && data.username}!
                </Typography>
                <NewPartida />   
              </CardContent>       
            </Card>
          </div>
        </Marco>
      </>
    )
}

export default Dashboard;