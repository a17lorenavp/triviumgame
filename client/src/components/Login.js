import React from 'react';
import Marco from './Marco';
import { Link } from 'react-router-dom';
import Header from './Header';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import NewUser from './NewUser';

const Login = ({ setIsLogged }) => {
    

    const [state, setState] = React.useState({
        username: '',
        password: ''
    });

    const login = async() => {
        const response = await fetch('http://localhost:5000/login', {
            method: 'POST',
            headers: {
                'Content-type': 'application/json',
            },
            body: JSON.stringify({
                username: state.username,
                password: state.password
            })
        });
            
        if(response.status === 200){
            const json = await response.json();
            localStorage.setItem('trivialToken', json.token);
            setIsLogged(true);
        }
    }

    return (
        <>
            <Header></Header>
            
            <Marco>      
                <div style={{
                    width:'100%',
                    display:'flex',
                    justifyContent:'center',
                    height:'100%'
                }}>
                    <Card style={{
                        marginTop:'1.5em',
                        width:'60%', 
                        height:'calc(100% - 3em)'
                    }}>
                        <CardContent style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent:'center',
                        }}>
                            <Typography variant="h3" gutterBottom style={{color: '#386F85'}}>
                                Bienvenido
                            </Typography>
                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <TextField
                                        label="Nombre"
                                        fullWidth
                                        defaultValue="Default Value"
                                        variant="outlined"
                                        value={state.username} 
                                        onChange={event => setState({
                                            ...state, 
                                            username: event.target.value
                                    }) }
                                    />
                                </Grid>
                            </Grid>
                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <TextField
                                        label="Contraseña"
                                        fullWidth
                                        type='password'
                                        defaultValue="Default Value"
                                        variant="outlined"
                                        value={state.password} 
                                        onChange={event => setState({
                                            ...state, 
                                            password: event.target.value
                                        })}
                                    />
                                 </Grid>
                            </Grid>
                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <Button  variant="contained" size="large" color="primary" onClick={login} fullWidth>LOGIN</Button>
                                </Grid> 
                            </Grid>  
                            <Grid container style={{
                                marginTop:'1em'
                            }}>
                                <Grid item lg={3}></Grid>
                                <Grid item xs={12} lg={6}>
                                    <Link className='link' to='/newUser'>
                                        <Button  variant="contained" size="large" color="primary" fullWidth>NUEVO USUARIO</Button>
                                    </Link>
                                    
                                </Grid> 
                            </Grid>    
                        </CardContent>
                            
                    </Card>      
                </div>
                
            </Marco>
        </>
    );
}



export default Login;