import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';


const Pregunta = ({ match, partida }) => {
    const [data, setData] = React.useState(null);
    const [resultado, setResultado] = React.useState(null);

    const checkAnswer = async datos => {
        const response = await fetch(`http://localhost:5000/partida/${partida._id}/pregunta`, {
            headers: {
                'token-jibiri': localStorage.getItem('trivialToken'),
                'content-type': 'application/json'
            }, 
            method:'POST',
            body: JSON.stringify({respuesta:datos, idPregunta: data._id})
          });

          if(response.status === 200){
            const json = await response.json();
            setResultado({mensaje:json.message, respuesta:datos});
          }
    }

    const getDatos = async () => {
        const response = await fetch(`http://localhost:5000/partida/${partida._id}/pregunta`, {
            headers: {
              'token-jibiri': localStorage.getItem('trivialToken')
            }
          });

          if(response.status === 200){
            const json = await response.json();
            setData(json);
          }
    }

    React.useEffect(() => {
        getDatos();
    }, [])
    if(!data){
        return 'loading';
    }
    console.log(data);
    return (
        <>
            <div>
                {data.pregunta}
            </div>
            <div style={{
                display: 'flex', 
                flexDirection: 'column',
                alignItems: 'center', 
                marginTop: '10px'
            }}>
                {
                    data.opciones.map(datos => (
                        <Button variant="contained" size="large" style={{
                            width : '40em',
                            marginTop: '5px',
                            backgroundColor: (resultado && resultado.respuesta === datos) ?
                                resultado.mensaje === 'Correcto!' ? 'green' : 'red' : 'gainsboro'
                        }} onClick={() => checkAnswer(datos)}>{datos}</Button>  
                    ))                
                }
            </div>
         </>
    )
}

export default Pregunta;