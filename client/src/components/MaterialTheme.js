import React from 'react';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({

  overrides: {

  },
  palette: {
    type: 'light',
    primary: {
        // light: will be calculated from palette.primary.main,
        main: '#386F85',
        // dark: will be calculated from palette.primary.main,
        // contrastText: will be calculated to contrast with palette.primary.main
      },
  },
});

export default function GlobalThemeOverride({children}) {
  return (
    <ThemeProvider theme={theme}>
        {children}
    </ThemeProvider>
  );
}