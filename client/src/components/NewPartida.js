import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const NewPartida = ({ match, history }) => {

    const [success, setSuccess] = React.useState(false);

    const createPartida = async () => {
        const response = await fetch('http://localhost:5000/newPartida', {
          method: 'POST',
          headers: {
            'token-jibiri': localStorage.getItem('trivialToken'),
            'Content-type': 'application/json',
          },
        });

        if(response.status === 200){
          const json = await response.json();
          history.push(`/partida/`+ json._id);
        }
    }
    return (
        <>
        <Grid container style={{
            marginTop:'1em'
        }}>
            <Grid item lg={3}></Grid>
            <Grid item xs={12} lg={6}>
                <Button  variant="contained" size="large" color="primary" onClick={createPartida} fullWidth>NUEVA PARTIDA</Button>
            </Grid> 
        </Grid> 
        </>
    )
}

export default withRouter(NewPartida);