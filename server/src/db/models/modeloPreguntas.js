module.exports = db => {
   db.createCollection("preguntas", {
      validator: {
         $jsonSchema: {
            bsonType: "object",
            required: ["categoria", "pregunta", "opciones", "respuesta" ],
            properties: {
               id: {
                  bsonType: "int",
               },
               categoria: {
                  enum: [ "Naturaleza", "Geografia", "Historia", "Espectaculos", "Deportes", "Arte y literatura" ],
                  description: "Naturaleza, Geografia, Historia, Espectaculos, Deportes, Arte y literatura"
               },
               pregunta: {
                  bsonType: "string",
               },
               opciones: {
                  bsonType: "array"
               },
               respuesta: {
                  bsonType: "string",
               }
            }
         }
      }
   })
}

// const collectionPreguntas = db.collection("preguntas");
//     collectionPreguntas.insertMany(
//             [{
//         id: 1,
//         categoria: 'Espectaculos',
//         pregunta: '¿Cuál era el número favorito de Coco Channel?',
//         opciones: ['cinco', 'cuatro', 'tres'],
//         respuesta: 'cinco'
//     }, {
//         id: 2,
//         categoria: 'Naturaleza',
//         pregunta: 'Después de españa, ¿qué país es el segundo mayor productor mundial de aceite de oliva?',
//         opciones: ['Portugal', 'Italia', 'Grecia'],
//         respuesta: 'Italia'
//     }, {
//         id: 3,
//         categoria: 'Geografia',
//         pregunta: '¿Qué capital europea tiene una estatua dedicada a la Sirenita de Hans Christian Andersen?',
//         opciones: ['Berlín', 'Copenhague', 'Estocolmo'],
//         respuesta: 'Copenhague'
//     }, {
//         id: 4,
//         categoria: 'Historia',
//         pregunta: '¿En qué comunidad autónoma nacieron los conquistadores Francisco Pizarro, Hernán Cortés y Vasco Núñez de Balboa?',
//         opciones: ['Castilla y León', 'Madrid', 'Extremadura'],
//         respuesta: 'Extremadura'
//     }]);