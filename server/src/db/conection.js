const { MongoClient } = require('mongodb');
const Usuarios = require('./models/modeloUsuarios');
const Partidas = require('./models/modeloPartidas');
const Preguntas = require('./models/modeloPreguntas');

//afe004d9-12aa-4789-a741-226de499a3c1
// Nombre de bd
const dbName = 'Triv';
// Conexión URL (estas corriendo en local :D)
const url = 'mongodb+srv://admin:jibiri@triv-jd9rj.mongodb.net/Triv?retryWrites=true&w=majority';

const client = new MongoClient(url, {
  useUnifiedTopology: true
});

let db;

const initModels = db => {
    Usuarios(db);
    Partidas(db);
    Preguntas(db);
}


const getTrivialDB = new Promise(async resolve => {
    try{
        if(!client.topology){
            await client.connect();
            initModels(client.db('trivial'));
        }
    
        db = client.db("trivial");
        resolve(db);
    }catch(error){console.log(error)}

})



module.exports = {
    getTrivialDB
}

