const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const { MongoClient, ObjectId } = require('mongodb');
const { getTrivialDB } = require('./src/db/conection');

let db;
(async () => {
    db = await getTrivialDB;
    
})();

const app = express();

app.use(cors());
app.use(bodyParser.json({
    extended: false,
    parameterLimit: 500000,
    limit: '50mb'
}));
// Request query parser
app.use(bodyParser.urlencoded({
    extended: true,
    limit: '150mb',
    parameterLimit: 500000
}));

app.post("/login", async(req, res) => {
    const { username, password } = req.body;
    const user = await db.collection("usuarios").findOne({username : username});

    if(!user){
        return res.status(404).json({ message: 'User not found'});
    }

    if(user.password === password) {
        return res.status(200).json({ token: user._id});
    }

    return res.status(401).json({ message: 'Invalid password'});

});

const getUserById = async id => {
    return db.collection("usuarios").findOne(ObjectId(id));
}

const getPreguntaById = async id => {
    return db.collection("preguntas").findOne(ObjectId(id));
}

app.get("/partidas", async (req, res) => {
    const user = await getUserById(req.headers['token-jibiri']);

    const partidasUser = await db.collection("partidas").find({ userId: user._id }).toArray();
    return res.status(200).json({
        partidas: partidasUser
    })
});

const getPartidasById = async id => {
    return db.collection("partidas").findOne(ObjectId(id));

}
app.get("/partida/:idPartida", async (req, res) => {
    const partida = await getPartidasById(req.params.idPartida);
    if(!partida){
        return res.status(404).json({message: 'Not found'});
    }
    return res.status(200).json(partida);
});

app.get("/partida/:idPartida/pregunta", async (req, res) => {
    const pregunta = await db.collection("preguntas").findOne()
    return res.status(200).json(pregunta);
});


app.post("/partida/:idPartida/pregunta", async (req, res) => {
    try{
        const pregunta = await getPreguntaById(req.body.idPregunta);
        if(req.body.respuesta === pregunta.respuesta){
            return res.status(200).json({
                message: 'Correcto!'
            });
        }else{
            return res.status(200).json({
                message: 'Has fallado!'
            });
        }
    }catch(error){
        res.status(500).json({message: 'error'});
    }
});

app.get("/perfil", async (req, res) => {
    const user = await getUserById(req.headers['token-jibiri']);

    if(!user){
        return res.status(404).json({ message: 'Not authorized' });
    }

    return res.status(200).json(user); 
});

app.post("/newUser", async (req, res) => {
    const usuarios = db.collection("usuarios");
    let user = req.body;

    await usuarios.insertOne(user);

    return res.status(200).json(user);
});

app.post("/newPartida", async (req, res) => {
    const user = await getUserById(req.headers['token-jibiri']);
    console.log(user);
    const partidas = db.collection("partidas");

    let newPartida = {
        userId: user._id,
        creationDate: new Date().toISOString(),
        state: 0,
        puntuacion: 0,
    };

    await partidas.insertOne(newPartida);

    return res.status(200).json(newPartida);

});



app.listen(5000, () => console.log('Server arrancado en el puerto 5000'));